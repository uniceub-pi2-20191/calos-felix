import React, {Component} from 'react';
import {createStackNavigator,createAppContainer} from 'react-navigation';

import Login from "./src/components/Login/Login"
import Cadastro from "./src/components/Cadastro/Cadastro"
import Mapa from "./src/components/Mapa/Mapa"

const MainNavigator = createStackNavigator({

  Tela1: {screen: Login},
  Tela2: {screen: Cadastro},
  Tela3: {screen: Mapa},

 
},
{
  initialRouteName:'Tela1'
});

const AppContainer =  createAppContainer(MainNavigator);

export default class App extends Component{
  render() {
    return <AppContainer/>;
      
  }
}