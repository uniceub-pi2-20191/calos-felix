import React, {Component} from 'react';
import {Text, View, Button,Image} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Cadastro from "./src/Cadastro";
import Login from "./src/Login";

class TelaInicial extends Component{
  render () {
    return(
      <View style={{flex:1, justifyContent: "center", backgroundColor: '#4682B4'}}>
        <View style = {{alignItems: "center"}}>
           <Image style={{width:100, height: 100}}source={require('./src/image/Logo.png')}/>
              <Text style={{fontSize: 20, color: '#DCDCDC', fontSize: 30}}>Carona Top</Text>
                </View>
              <View style ={{margin:20}}>
                <Button title = "Fazer Login" onPress = { () => this.props.navigation.navigate("Login")}/>
              </View> 
            </View>
          );
        }
      }

class TelaCadastro extends Component{
  render () {
    return(
      <View style={{flex:1, backgroundColor: '#4682B4'}}>
        <View style = {{alignItems: "center"}}>
          <Text style={{fontSize: 30, color: '#DCDCDC'}}>Cadastro de Usuário</Text>
          <Image style={{width:100, height: 100}}source={require('./src/image/Logo.png')}/>
           <Text style={{fontSize: 20, color: '#DCDCDC', fontSize: 30}}>Carona Top</Text>
          <Cadastro/>
        </View>
      </View>
     );
  }
}

class TelaLogin extends Component{
  render () {
    return(
      <View style={{flex:1, backgroundColor: '#4682B4'}}>
        <View style = {{alignItems: "center"}}>
          <Text style={{fontSize: 30, color: '#DCDCDC'}}>Login</Text>
          <Image style={{width:100, height: 100}}source={require('./src/image/Logo.png')}/>
           <Text style={{fontSize: 20, color: '#DCDCDC', fontSize: 30}}>Carona Top</Text>
            <Login/>
             </View>
            <View style ={{margin: 20, marginTop:90}}>
          <Text>Não Tem Cadastro?</Text>
        <Button
            title = "Cadastre-se"
            onPress = { () => this.props.navigation.navigate("Cadastro")}/>
          </View> 
       </View>
      );
  }
}
const Navegacao = createStackNavigator (
  {
    Inicio: {
      screen: TelaInicial
    },
    Cadastro: {
      screen: TelaCadastro
    },
    Login: {
      screen: TelaLogin
    }
  },
  {
    initialRouteName: "Inicio"
  }
);

const AppContainer = createAppContainer(Navegacao);

export default class App extends Component{
  render() {
    return <AppContainer/>;
      
  }
}