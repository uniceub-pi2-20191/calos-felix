import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity,Alert} from 'react-native';



export default class Cadastro extends Component<{}> {

 constructor(props) {
      super(props);
      this.state = { nome: '', email: '', pass: ''};
  }

  onPressButton() {
    Alert.alert(this.state.email)
  }

   setNome(nome) { 
    this.state.nome = nome
  }

  setEmail(email) {
    this.state.email = email
  }

  setPass(pass) {
    this.state.pass = pass
  }  


  render() {


    return (
       <View style={styles.container}>

       <TextInput style={styles.input}
               onChangeText={(text) => this.setNome(text)}
               autoCapitalize='none'
               autoCorrect={false}
               placeholder='Nome Completo'
               placeholderColor='rgba(255,255,255,0.7)'
        />

        <TextInput style={styles.input}
               onChangeText={(text) => this.setEmail(text)}
               autoCapitalize='none'
               autoCorrect={false}
               keyboardType='email-address'
               placeholder='Email'
               placeholderColor='rgba(255,255,255,0.7)'
        />
        <TextInput style={styles.input}
               placeholder='Pass'
               onChangeText={(text) => this.setPass(text)}
               placeholderColor='rgba(255,255,255,0.7)'
               secureTextEntry/>
        <TouchableOpacity style={styles.button} onPress={() => this.onPressButton()}>
          <Text style={styles.textButton}> OK </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({

  container: {
    padding: 20
  },

  input: {

    height: 60,
    width: 300,
    borderRadius: 5,
    margin: 2,
    backgroundColor: '#ADD8E6',
    padding: 15,

  },

  button: {
    height: 60,
    width: 300,
    padding: 3,
    borderRadius: 5,
    margin: 2,
    backgroundColor: '#A9A9A9'

  },

  textButton: {
    color: '#C0C0C0',
    textAlign: 'center',
    fontWeight: '300',
    fontSize: 30

  }

});